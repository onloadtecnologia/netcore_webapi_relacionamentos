namespace teste_api.ModelsDTO
{
    public class FornecedorDTO
    {

        public int FornecedorId {get;set;}
        public string Nome  {get;set;}
        public string Email {get;set;}
        
    }
}
using Microsoft.EntityFrameworkCore;
using teste_api.Models;

namespace teste_api.Database
{
    public class DatabaseContext:DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options):base(options)
        {
            
        }
        public DbSet<Fornecedor> fornecedor {get;set;}
        public DbSet<Produto> produto {get;set;}

    }
}
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using teste_api.Database;
using teste_api.Models;

namespace teste_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProdutoController:ControllerBase
    {
        private readonly DatabaseContext db;

        public ProdutoController(DatabaseContext _db)
        {
            db = _db;

        }

        [HttpGet]
        public async Task<ActionResult<List<Produto>>> findAll()
        {
            var dados = await db.produto.ToListAsync();
            return Ok(dados);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Produto>> findById(int id)
        {
            var dados = await db.produto.FindAsync(id);
            if(dados !=null)
            {
                return Ok(dados);
            }
            return NotFound("produto não encontrado:(");
        }

        [HttpPost]
        public async Task<ActionResult<Produto>> save(Produto produto)
        {
            var fornecedor = await db.fornecedor
            .Include(f => f.Produtos)
            .Where(f => f.FornecedorId == produto.FornecedorId)
            .FirstOrDefaultAsync();
            produto.Fornecedor = fornecedor;
            var newProduto = await db.produto.AddAsync(produto);
            int i = await db.SaveChangesAsync();
            if(i > 0)
            {
                return CreatedAtAction(nameof(save),produto);
            }
            return BadRequest("desculpe houve um erro:(");

        }


    }
}
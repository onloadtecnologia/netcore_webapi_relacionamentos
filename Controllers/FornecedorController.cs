using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using teste_api.Database;
using teste_api.Models;
using teste_api.ModelsDTO;

namespace teste_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FornecedorController : ControllerBase
    {
        private readonly DatabaseContext db;

        public FornecedorController(DatabaseContext _db)
        {
            db = _db;

        }
        [HttpGet]
        public async Task<ActionResult<List<Fornecedor>>> findAll()
        {
            var dados = await db.fornecedor.Include(f => f.Produtos).ToListAsync<Fornecedor>();
            return Ok(dados);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Fornecedor>> finById(int id)
        {
            var fornecedor = await db.fornecedor.FindAsync(id);
            
            if (fornecedor != null)
            {
                return Ok(fornecedor);

            }
            return BadRequest("fornecedor não encontrado");

        }

        [HttpPost]
        public async Task<ActionResult<FornecedorDTO>> save(Fornecedor fornecedor)
        {
            FornecedorDTO dto = new FornecedorDTO();
            var new_fornecedor = await db.fornecedor.AddAsync(fornecedor);
            int i = await db.SaveChangesAsync();
            if (i > 0)
            {
                dto.FornecedorId = fornecedor.FornecedorId;
                dto.Nome = fornecedor.Nome;
                dto.Email = fornecedor.Email;
            }
            return CreatedAtAction(nameof(save), dto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Fornecedor>> update(int id, Fornecedor fornecedor)
        {
            var old_fornecedor = await db.fornecedor.FindAsync(id);

            if (old_fornecedor != null)
            {
                old_fornecedor.Nome = fornecedor.Nome;
                old_fornecedor.Telefone = fornecedor.Telefone;
                old_fornecedor.Email = fornecedor.Email;
                await db.SaveChangesAsync();
                return CreatedAtAction(nameof(update), fornecedor);
            }
            return BadRequest("fornecedor não encontrado");
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Fornecedor>> deleteById(int id)
        {
            var _del = await db.fornecedor.FindAsync(id);
            if (_del != null)
            {
                var del_fornecedor = db.fornecedor.Remove(_del);
                await db.SaveChangesAsync();
                return Ok(_del);

            }        
            return BadRequest("fornecedor não encontrado");

        }

    }
}
using System.Text.Json.Serialization;

namespace teste_api.Models
{
    public class Produto
    {
        
        public int ProdutoId {get;set;}
        public string Nome  {get;set;}
        public string Descricao  {get;set;}
        public int FornecedorId {get;set;}
        [JsonIgnore]
         public Fornecedor? Fornecedor {get;set;}
    }
}
